#Exercício 1
entrada = [1,3,5]
soma = []
subtracao = []
multiplicacao = []
divisao = []
print (entrada.to_s + "\n") 
entrada.map!(&:to_s)
print entrada
puts ""

#Exercício 2
entrada2 =[[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
for i in (0...entrada2.length)
  sum = [entrada2[i][0]]
  for j in (1...entrada2[i].length)
    sum[0] += entrada2[i][j]
  end
  soma.push(sum)
end
print soma
puts ""

for i in (0...entrada2.length)
  sub = [entrada2[i][0]]
  for j in (1...entrada2[i].length)
    sub[0] -= entrada2[i][j]
  end
  subtracao.push(sub)
end
print subtracao
puts ""

for i in (0...entrada2.length)
  mul = [entrada2[i][0]]
  for j in (1...entrada2[i].length)
    mul[0] *= entrada2[i][j]
  end
  multiplicacao.push(mul)
end
print multiplicacao
puts ""

for i in (0...entrada2.length)
  div = [entrada2[i][0].to_f]
  for j in (1...entrada2[i].length)
    div[0] /= entrada2[i][j]
  end
  div[0] = div[0].round(3)
  divisao.push(div)
end
print divisao
puts ""

#Exercício 3
hashEntrada = {chave1:5, chave2:30, chave3:20}
arrHash = hashEntrada.values
arrHash.map!{|integer| integer**2}
print arrHash

puts ""
#Exercício 4
entrada3 = [3, 6, 7, 8]
arrDivisivel3 = []
entrada3.each do |int|
  if int%3==0 then arrDivisivel3.push(int) end
end
print arrDivisivel3